﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testTaskApiConsole
{
    public class ProductDto:Dto
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
    }
}
