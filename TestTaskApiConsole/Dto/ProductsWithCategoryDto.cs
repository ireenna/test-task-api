﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testTaskApiConsole;

namespace TestTaskApiConsole.Dto
{
    public class ProductsWithCategoryDto
    {
        public List<ProductDto> Products { get; set; }
        public List<CategoryDto> Categories { get; set; }

    }
}
