﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace testTaskApiConsole
{
    public class CategoryDto:Dto
    {
        public string Name { get; set; }
    }
}
