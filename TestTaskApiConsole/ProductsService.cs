﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TestTaskApiConsole.Dto;

namespace testTaskApiConsole.Controllers
{
    public class ProductsService
    {
        private static readonly HttpClient client = new HttpClient();
        private static readonly string localpath = $"https://tester.consimple.pro/";
        public static async Task<ProductsWithCategoryDto> GetProductsWithCategories()
        {
            var response = await client.GetAsync(localpath);
            string strResponse = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ProductsWithCategoryDto>(strResponse);
        }
    }
}
