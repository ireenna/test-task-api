﻿using ConsoleTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testTaskApiConsole;
using testTaskApiConsole.Controllers;
using TestTaskApiConsole.Dto;

namespace TestTaskApiConsole
{
    class Program
    {
        static async Task Main(string[] args)
        {
            do
            {
                try
                {
                    Console.Clear();
                    Console.Write("Options:\n" +
                        "1. Get the table.\n" +
                        "2. Exit.\n" +
                        "Choose an option: ");

                    int operation = Convert.ToInt32(Console.ReadLine());
                    switch (operation)
                    {
                        case 1:
                            var productsWithCateg = await ProductsService.GetProductsWithCategories();

                            var query = productsWithCateg.Products.Join(productsWithCateg.Categories,
                                p => p.CategoryId,
                                c => c.Id, 
                                (p, c) => (p.Name, c.Name));

                            var table = new ConsoleTable("Product", "Category");
                            foreach (var item in query)
                            {
                                table.AddRow(item.Item1, item.Item2);
                            }
                            Console.WriteLine(table);
                            break;
                        case 2:
                            Environment.Exit(0);
                            break;
                        default:
                            throw new FormatException();
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Sorry, you wrote wrong command. Please, try again.");
                }
                catch
                {
                    Console.WriteLine("Oops! Something went wrong. Please, try again.");
                }
                    Console.ReadLine();
            }
            while (true);
        }
    }
}
